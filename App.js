import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Platform } from 'react-native';
import TwilioVoice from 'react-native-twilio-voice-sdk'
import { PermissionsAndroid } from 'react-native';

// access token from server, lasts for 1 hour
const accessToken = "";
const callNumber = '';
export default function App() {

  const requestMicrophonePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          {
            'title': 'Microphone Permission',
            'message': 'App needs access to you microphone ' +
                'so you can talk with other users.'
          }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the microphone")
      } else {
        console.log("Microphone permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  const checkMicrophonePermission = () => {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO).then((result)=>{
      if(!result){
        requestMicrophonePermission();
      }
    });
  }

  if (Platform.OS === 'android'){checkMicrophonePermission();}

  const [connectedCall, setConnectedCall] = useState(null);
  const [ringing, setRinging] = useState(false);

  // listener on connect event
  TwilioVoice.on("connect", connectedCall => {
    setConnectedCall(connectedCall)
    setRinging(false)
  })
  // listener on ringing event
  TwilioVoice.on("ringing", () => setRinging(true))
  // listener on disconnect event
  TwilioVoice.on("disconnect", () => setConnectedCall(null))

  const callTwilio = () => TwilioVoice.connect(accessToken, {to: callNumber})
  const hangUpTwilio = () => connectedCall.disconnect()

  return (
    <View style={styles.container}>
      <Text>TWILIO POC</Text>
      {ringing && <Text style={styles.ringingText}>RINGING....</Text>}
      {!connectedCall && <TouchableOpacity style={[styles.button, styles.callButton]} onPress={() => callTwilio()}>
        <Text style={styles.buttonText}>CALL</Text>
      </TouchableOpacity>}
      {!!connectedCall && <TouchableOpacity style={[styles.button, styles.hangUpButton]} onPress={() => hangUpTwilio()}>
        <Text style={styles.buttonText}>HANG UP</Text>
      </TouchableOpacity>}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    marginTop: 20,
    borderRadius: 20,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold'
  },
  callButton: {
    backgroundColor: '#07b335',
  },
  hangUpButton: {
    backgroundColor: '#b30707',
  },
  ringingText: {
    color: '#1007b3',
    marginTop: 20
  },
});
