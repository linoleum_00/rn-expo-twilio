export default class TwilioVoiceMock {

    static on() {
        return "on"
    }
    static connect() {
        return "connected"
    }
}