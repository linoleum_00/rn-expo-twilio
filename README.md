### **React Native with Expo and Twilio POC**

This small project was born as a proof of concept to determine if it is possible to add third party libraries
in an expo project, without losing the goods of the expo environment, but using libraries such as a Twilio SDK 
which requires to use Native modules and still deploy the app in stores.


To accomplish this, we are using the babel module resolver, to add an alias upon the react-native-twilio-voice-sdk
library and use a mock of the main class instead, but only if the app is ran in un-ejected mode.
Also, a couple of new npm scripts to switch between ejected and un-ejected modes.


To install dependencies, run in the terminal:

`npm install`

For ios, don't forget to run:

`cd ios && pod install && cd ..`

 To run the app within the expo environment:
 
 `npm run ios-expo`
 
 To run the native app:
 
 `npm run ios-native`
 