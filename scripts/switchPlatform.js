const fs = require('fs');
const path = require('path');
const isEjected = process.env.REACT_NATIVE_EJECTED === 'true';
fs.writeFileSync(path.resolve(__dirname, '../config.json'), JSON.stringify({ isEjected }, null, "\t"));
console.log(`Updated config.json file with isEjected: ${isEjected}`);